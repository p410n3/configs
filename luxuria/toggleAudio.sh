#!/bin/bash
set -euo pipefail
#pactl list cards short
headphonesID=$(pactl list cards short | grep HyperX | awk '{ print $1 }')
speakersID=$(pactl list cards short | grep "alsa_card.pci-0000_00_1b.0" | awk '{ print $1 }')
# If the current sink contains HyperX then it's my headphones, so switch it to speakers. If not do the opposite
if pactl list short sinks | grep "HyperX" > /dev/null; then
	pactl set-card-profile $speakersID output:analog-stereo
	pactl set-card-profile $headphonesID off
	echo "Toggled from Headset to Speakers."
else
	pactl set-card-profile $headphonesID output:analog-stereo+input:iec958-stereo
	pactl set-card-profile $speakersID off
	echo "Toggled from Speakers to Headset."
fi