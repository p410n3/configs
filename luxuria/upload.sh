#!/bin/bash
host="luxuria.rys.pw"
rsync -e "ssh -p 9595" ./fcrontab c0rn3j@$host:/tmp/fcrontab
ssh -p 9595 c0rn3j@$host fcrontab /tmp/fcrontab
rsync -e "ssh -p 9595" ./toggleAudio.sh ./uploadPS3saves.sh c0rn3j@$host:/home/c0rn3j/
rsync -e "ssh -p 9595" ./smb.conf root@$host:/etc/samba/smb.conf
rsync -e "ssh -p 9595" ./nginx.conf root@$host:/etc/nginx/nginx.conf
ssh -p 9595 root@$host "nginx -t && systemctl restart nginx"
rsync -e "ssh -p 9595" ./ps3net.service root@$host:/etc/systemd/system/ps3net.service
rsync -e "ssh -p 9595" ./MyBridge.netdev ./MyBridge.network ./MyEth.network root@$host:/etc/systemd/network/
