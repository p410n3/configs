#!/bin/bash
host="sc2.rys.pw"
rsync -avz ./nginx.conf root@$host:/etc/nginx/nginx.conf
ssh root@$host "nginx -t && systemctl restart nginx"
rsync -avz ./fcrontab root@$host:/tmp/fcrontab
ssh root@$host fcrontab /tmp/fcrontab
rsync -avz ./updateGitWiki.sh ./updateGitPrivatebin.sh ./certrenew.sh ./smokeping root@$host:/root/
rsync -avz ./smokeping root@$host:/etc/
ssh root@$host "systemctl restart smokeping"
