#!/bin/bash
# sudo certbot certonly --standalone -d "rpi3.c0rn3j.com, pihole.c0rn3j.com, esp8266.c0rn3j.com, cloud.c0rn3j.com" --email spleefer90@gmail.com --rsa-key-size 4096 --agree-tos
set -euo pipefail
/usr/bin/systemctl daemon-reload
/usr/bin/systemctl stop nginx
/usr/bin/certbot renew --force-renew
/usr/bin/systemctl start nginx
