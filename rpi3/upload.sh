#!/bin/bash
host="rpi3.rys.pw"
rsync -e "ssh -p 9598" ./nginx.conf root@$host:/etc/nginx/nginx.conf
ssh -p 9598 root@$host "nginx -t && systemctl restart nginx"
rsync -e "ssh -p 9598" ./fcrontab root@$host:/tmp/fcrontab
ssh root@$host -p 9598 fcrontab /tmp/fcrontab
rsync -e "ssh -p 9598" ./backupscript.sh ./wifiAPscript.sh ./certrenew.sh ./rollers.sh root@$host:/root/