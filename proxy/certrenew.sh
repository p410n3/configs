#!/bin/bash
# sudo certbot certonly --standalone -d "proxy.rys.pw, cloud.c0rn3j.com, cloud.rys.pw, rss.rys.pw, wekan.rys.pw" --email spleefer90@gmail.com --rsa-key-size 4096 --agree-tos
set -euo pipefail
/usr/bin/systemctl daemon-reload
/usr/bin/systemctl stop nginx
/usr/bin/certbot renew --force-renew
/usr/bin/systemctl start nginx
