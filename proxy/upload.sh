#!/bin/bash
host="proxy.rys.pw"
rsync -e "ssh -p 9600" ./nginx.conf root@$host:/etc/nginx/nginx.conf
ssh -p 9600 root@$host "nginx -t && systemctl restart nginx"
rsync -e "ssh -p 9600" ./certrenew.sh root@$host:/root/