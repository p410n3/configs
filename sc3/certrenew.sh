#!/bin/bash
#sudo certbot certonly --nginx -d "sc3.rys.pw, text.rys.pw, a4.rys.pw, ip.rys.pw, ipv4.rys.pw, ipv6.rys.pw, speedtest.rys.pw, mcskins.rys.pw" --email spleefer90@gmail.com --rsa-key-size 4096 --agree-tos
set -euo pipefail
/usr/bin/systemctl daemon-reload
/usr/bin/systemctl stop nginx
/usr/bin/certbot renew --force-renew
/usr/bin/systemctl start nginx