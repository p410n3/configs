#!/bin/bash
host="sc3.rys.pw"
rsync -avz ./nginx.conf root@$host:/etc/nginx/nginx.conf
ssh root@$host "nginx -t && systemctl restart nginx"
rsync -avz ./crontab root@$host:/tmp/crontab
ssh root@$host crontab /tmp/crontab
rsync -avz ./certrenew.sh root@$host:/root/
rsync -avz ./a4 ./mcskins ./text ./tradechecker ./ipdetect.js ./ipdetect.php root@$host:/var/www/html/
