<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
		<!--We want monospaced text to see characters where they should be, right under each other.
    We also want no text wrapping.
-->
<style>
			html {
				font-family: monospace;
				white-space: nowrap;
			}
			.pointer {
				cursor: pointer;
			}
			.monospace {
				font-family: monospace;
				white-space: nowrap;
			}
			.popover {
				max-width: none;
			}
			.popover-body {
				white-space: pre-wrap;
				font-family: monospace;
			}
		</style>
		<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
		<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
	</head>

	<body>
		<div class="container">
			<div class="row">
				<div class="col-xs-12">
					<p>This script is available under the AGPL version 3 license on: <a href="https://gitlab.com/C0rn3j/configs/tree/master/sc3/a4">https://gitlab.com/C0rn3j/configs/tree/master/sc3/a4</a>
					<br>Made for <a href="https://gitlab.com/C0rn3j/a4tech_bloody_p85_driver">this</a> project.
					<br>To process the pcapng files into text this form understands, you'll need the following script: <a href="https://gitlab.com/C0rn3j/configs/tree/master/sc3/a4/a4.sh">https://gitlab.com/C0rn3j/configs/tree/master/sc3/a4/a4.sh</a>
					<br>
					<br>Queries are marked blue (host>device), responses are marked green (device>host).
					<br>Predefined rules in the index.php files are applied to the protocol and known/decrypted info is in a box that appears when you hover over the line.
					<br>
					<br><a href="https://gitlab.com/C0rn3j/configs/raw/master/sc3/a4/parsedexample.txt">Example a4.sh output</a>
					<br>Enter output from the a4.sh parser:</p>
					<form action="" method="post" >
						<textarea rows="5" cols="80" name="cmd"> </textarea><br>
						<input class="btn btn-default" type="submit" value="Submit"/>
					</form><br>
		<?php
		ini_set('display_startup_errors', 1);
		ini_set('display_errors', 1);
		error_reporting(-1);

		if(!isset($_POST['cmd']))
			exit();
		$input=$_POST['cmd'];
		$inputarray = preg_split("/\\r\\n|\\r|\\n/", $input);
		$color="#000000";
		$protocolType="N/A";
		foreach($inputarray as $row) {
			if (strpos($row, 'SUBMIT') !== false) {
				$color="#000080";
				$protocolType="Query";
			}
			if (strpos($row, 'COMPLETE') !== false) {
				$color="#006400";
				$protocolType="Response";
			}
			if (strpos($row, 'CONTROL') !== false || strpos($row, 'INTERRUPT') !== false || strpos($row, 'BULK') !== false) {
				continue; // Line has a string we're ignoring, therefore do not echo it out
			}
			list($tooltipTitle,$tooltipText) = protocolDecode("A4Tech bloody", $row, $protocolType);
			// Echo the final line with tooltips and everything
			echo('<a class="pointer popoverData monospace" data-content="'.$tooltipText.'" data-placement="top" data-original-title="'.$tooltipTitle.'" data-trigger="hover" style="color:'.$color.';">'.$row.'</a><br/>');
		}
		//protocolDecode - takes a protocol (atm just supports 'A4tech bloody') and a protocol string like:
		//                 "0701 782c 5060 0000 0124 0826 4b28 0926 0240 0125 00c1 0298 00b8 56e0 0826 1428 0124 0826 5028 0926 ffff ffff ffff ffff ffff ffff ffff ffff ffff ffff ffff ffff";
		//                 If it matches a predefined rule, it returns a name of the function and a description
		function protocolDecode($protocol, $protocolString, $protocolType) {
			if($protocolType != "Query" && $protocolType != "Response") {
				return array ("N/A", "N/A");
			}
			if ($protocol = "A4Tech bloody") {
				switch ($protocolString) {
					case (preg_match('/^0705 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000/', $protocolString) ? true : false) :
						$decodedTitle = "Get some info";
						$decodedText = "Query:
$protocolString
Example response:
0705 0000 0000 0000 0100 e803 002a 002c 003e 5512 f03f 8b12 0000 0401 759f 2fe1 ffff 094a 0000 5aff ff07 8a5c feff 0000 0000 0000 0000 0000 0000 0000 0000 0000
                         BBCC                                         AAAA AAAA  DDD                 F         E
AAAA AAAA - Reversed Mouse ID. In the example: E12F9F75
BBCC - Number of trial clicks left (for unlicensed mice), BB goes down first and it underflows into CC. Starts at e8 03 which is a 1000.
DDD, E - DDD is fff during trial, 019 when licensed and 0ff when preactivated. E is e during trial, c when licensed and f when preactivated.
F - Current core. d for 1, c for 2, f for 3, e for 4 (for P series, seems to differ on other series)

Mouse ID is generated 'randomly' on first connect to bloody software. Mouse PID is then set based on Mouse ID.

What other information is contained is unknown.";
						break;
					case (preg_match('/^0705 .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... ..../', $protocolString) ? true : false) :
						$decodedTitle = "Get some info";
						$mouseID=substr($protocolString,77, 2).substr($protocolString,75, 2).substr($protocolString,72, 2).substr($protocolString,70, 2);
						$decodedText = "Response:
$protocolString
                         BBCC                                         AAAA AAAA  DDD                 F         E
AAAA AAAA - Reversed Mouse ID: $mouseID
BBCC - Number of trial clicks left (for unlicensed mice), BB goes down first and it underflows into CC. Starts at e8 03 which is a 1000.
DDD, E - DDD is fff during trial, 019 when licensed and 0ff when preactivated. E is e during trial, c when licensed and f when preactivated.
F - Current core. d for 1, c for 2, f for 3, e for 4 (for P series, seems to differ on other series)

Mouse ID is generated 'randomly' on first connect to bloody software. Mouse PID is then set based on Mouse ID.

What other information is contained is unknown.";
						break;
					case (preg_match('/^0711 .... 8... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... ..../', $protocolString) ? true : false) :
						$decodedTitle = "Set backlight intensity";
						$backlightIntensity=substr($protocolString,22, 1);
						$decodedText = "$protocolType:
$protocolString
          A         BB
A  - get/set bit. 0 means get, 8 means set.
BB - From 0 to 3, 0 is off, 3 is the brightest. Anything else means off.

Backlight intensity: $backlightIntensity";
						break;
					case (preg_match('/^0711 .... 0... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... ..../', $protocolString) ? true : false) :
						$decodedTitle = "Get backlight intensity";
						$backlightIntensity=substr($protocolString,22, 1);
						$decodedText = "$protocolType:
$protocolString
          A         BB
A  - get/set bit. 0 means get, 8 means set.
BB - From 0 to 3, 0 is off, 3 is the brightest. Anything else means off.

Backlight intensity: $backlightIntensity";
						break;
					case (preg_match('/^0703 0600 .... .... .1.. .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... ..../', $protocolString) ? true : false) :
						$decodedTitle = "Enter RGB edit mode";
						$decodedText = "$protocolType:
$protocolString
                AA
AA - set to 01 for edit mode(can control individual LEDs through commands) or to 00 to have the mouse modes running instead";
						break;
					case (preg_match('/^0703 0600 .... .... .0.. .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... ..../', $protocolString) ? true : false) :
						$decodedTitle = "Exit RGB edit mode";
						$decodedText = "$protocolType:
$protocolString
                AA
AA - set to 01 for edit mode(can control individual LEDs through commands) or to 00 to have the mouse modes running instead";
						break;
					case (preg_match('/^0703 0601 .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... ..../', $protocolString) ? true : false) :
						$decodedTitle = "Set constant/breathing light in RGB edit mode";
						$decodedText = "$protocolType:
$protocolString
               AABB
AA - 01 is breathing, 00 is static
BB - how fast is the breathing, from 01 to 04
     00 when AA is 00.

" ;
						break;
					case (preg_match('/^0703 0602 .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... ..../', $protocolString) ? true : false) :
						$decodedTitle = "Set RGB LEDs";
						$decodedText = "$protocolType:
$protocolString
                    1111 1122 2222 3333 3344 4444 5555 5566 6666 7777 7788 8888
                    Every LED has ff:ff:ff which controls R:G:B intensity";
						break;
					case (preg_match('/^0703 0605 .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... ..../', $protocolString) ? true : false) :
						$decodedTitle = "Get mouse mode";
						$decodedText = "$protocolType:
$protocolString
                    AABB CC

AA - Is always 03?
BB - Profile show mode byte - is 02 when in it, 00 when not in it.
CC - Which animation is the mouse set to if in RGB mode or RGB edit mode";
						break;
					case (preg_match('/^0710 .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... ..../', $protocolString) ? true : false) :
						$decodedTitle = "Set/Get RNG bytes";
						$decodedText = "$protocolType:
$protocolString
                    AAAA
AAAA - RNG bytes. Calling this sets them and they persist until firmware restart. The resulting bytes are based on time since the mouse firmware started running.
       They are used for various DRM functions, including generating Mouse ID and license activation." ;
						break;
					case (preg_match('/^0700 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... ..../', $protocolString) ? true : false) :
						$decodedTitle = "Restart firmware";
						$decodedText = "$protocolType:
$protocolString

Restarts firmware. Seems to be used when flashing kernel." ;
						break;
					case (preg_match('/^0702 00.. ..60 .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... ..../', $protocolString) ? true : false) :
						$decodedTitle = "Read data off an address";
						$decodedText = "$protocolType:
$protocolString
       AA BB        CCCC CCCC CCCC CCCC CCCC CCCC CCCC CCCC CCCC CCCC CCCC CCCC CCCC CCCC CCCC CCCC CCCC CCCC CCCC CCCC CCCC CCCC CCCC CCCC CCCC CCCC CCCC CCCC
AA BB - Address to read data from. BB overflows into AA.
CCCC CCCC C... - data

There's 112 chars per line, and 1 address position jumps 4 chars.
Meaning address X will give you data Y and X+1 is going to give you data Y +- 4 characters.
The data repeats itself after whole memory is read. The cutoff point is different for different chips (0x4000 for P93)" ;
						break;
					case (preg_match('/^0701 183f f460 .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... ..../', $protocolString) ? true : false) :
						$decodedTitle = "Set mouse ID";
						$decodedText = "$protocolType:
$protocolString
                    AAAA AAAA
AAAA AAAA - Mouse ID to set, reversed.

This function takes a lot more than just the mouse ID to work, but so far it is not known how exactly to get it working.
" ;
						break;
					case (preg_match('/^0701 78.. .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... ..../', $protocolString) ? true : false) :
						$decodedTitle = "Write data";
						$decodedText = "$protocolType:
$protocolString
       AA BBDD      CCCC CCCC CCCC CCCC CCCC CCCC CCCC CCCC CCCC CCCC CCCC CCCC CCCC CCCC CCCC CCCC
AA BB - Address to write data to. BB overflows into AA.
CCCC CCCC C... - Data to write
DD - This is e0 when BB is 00, 60 otherwise. It's also e0 when BB is 80 but has no effect and seems to work just as fine when set to 60.

This writes Fs from AA 10 to AA f0, which is 1024 characters. (as long as DD is e0)

WRITING DATA TO THE MOUSE CAN PERMANENTLY BRICK IT. DO NOT PLAY AROUND WITH THIS.
" ;
						break;
					case (preg_match('/^0701 0001 0060 .... 0000 .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... ..../', $protocolString) ? true : false) :
						$decodedTitle = "Write data - Set mouse to flashing mode";
						$decodedText = "$protocolType:
$protocolString
       AA BBDD      CCCC
AA BB - Address to write data to. BB overflows into AA.
CCCC - Data to write

This writes zeroes instead of a4a4 and makes mouse switch to flashing mode on firmware restart.

WRITING DATA TO THE MOUSE CAN PERMANENTLY BRICK IT. DO NOT PLAY AROUND WITH THIS.
" ;
						break;
					case (preg_match('/^0704 .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... ..../', $protocolString) ? true : false) :
						$decodedTitle = "??? Freezes colors";
						$decodedText = "$protocolType:
$protocolString
                          A
A - Setting this to 2 freezes RGB animation colors. Anything else unfreezes them. Software sets 1 for unfreeze.

Actual purpose and effect of this function is unknown.
Seems to be called for freezing before writing data and unfreezing after data was written (0701)
" ;
						break;
					case (preg_match('/^0713 .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... ..../', $protocolString) ? true : false) :
						$decodedTitle = "Get/Set mouse calibration";
						$decodedText = "$protocolType:
$protocolString
          A         BC
A - 8 set, 0 get.
B - 8 preset, c calibration
C - Calibration to get/set, from 0 to f, f being 1, 0 being 16.


" ;
						break;
					case (preg_match('/^070a .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... ..../', $protocolString) ? true : false) :
						$decodedTitle = "??? Happens after setting sensor calibration in bloody v6";
						$decodedText = "$protocolType:
$protocolString
????

" ;
						break;
					case (preg_match('/^070d .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... ..../', $protocolString) ? true : false) :
						$decodedTitle = "Set DPI";
						$decodedText = "$protocolType:
$protocolString
                    AABB CCCC   DD
AA   - How many more modes are enabled, from 00 to 04 (00 = 1 mode, 04 = 5 modes total)
BB   - mode selected, goes from 00 to 04 - this sets what mode is going to be next when you press the 'Next DPI mode' button.
CCCC - Value of Hz selected. It doesn't actually use the last 2 bytes on P series
       the value is calculated by the retarded bitmask algo and is different on various mice???
DD   - Refresh rate. 00 for 3Hz, 01 for 1000hz, 02 for 500Hz, 04 for 250Hz, 08 for 125Hz.
       Can be set to values like 03 for 666Hz[average] (1x 1000Hz, 1x 333Hz), which is something the v6 doesn't do (cause why would it)


" ;
						break;
					default:
						$decodedTitle = "Unknown";
						$decodedText = "Function unknown, could not decrypt";
						break;
				}
				return array ($protocolType.": ".$decodedTitle, $decodedText);
			}
		}
		?>
		<script>
			$(document).ready(function() {
				$('.popoverData').popover({ trigger: "hover" });
			});
		</script>
				</div>
		</div>
	</div>
	</body>
</html>
