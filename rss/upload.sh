#!/bin/bash
set -euo pipefail
host="rss.rys.pw"
rsync -6 ./nginx.conf root@$host:/etc/nginx/nginx.conf
ssh -6 root@$host "nginx -t && systemctl restart nginx"
rsync -6 ./fcrontab root@$host:/tmp/fcrontab
ssh -6 root@$host fcrontab /tmp/fcrontab
rsync -6 ./certrenew.sh root@$host:/root/